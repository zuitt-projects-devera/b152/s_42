let firstName = document.querySelector("#firstnameinput");
let lastName = document.querySelector("#lastnameinput");
let fullname = document.querySelector("#fullnamedisplay");


const displayFullName = () =>  {
	console.log(firstName.value);
	fullname.innerHTML = `${firstName.value} ${lastName.value}`;
	console.log(lastName.value);
}

const alertFullName = () =>  {
	if(firstName.value === "" || lastName.value === "") {
		alert(`Please complete the fields.`);
		firstName.value = "";
		lastName.value = "";
		fullname.innerHTML = "";
	}
	else {
		if(firstName.value.length <= 1 || lastName.value.length <= 1) {
			alert(`${fullname.innerHTML} is an invalid name.`);	
		}
		else {
			alert(`Thank you for registering ${fullname.innerHTML}`);	

			firstName.value = "";
			lastName.value = "";
			fullname.innerHTML = "";
			window.location.assign("https://zuitt.co");
		}
	}
}

firstName.addEventListener('keyup', displayFullName);
lastName.addEventListener('keyup', displayFullName);
submitbtn.addEventListener('click', alertFullName);