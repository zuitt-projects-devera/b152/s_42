//console.log("Hello World!");
/*
		JS DOM - Javascript Document Object Model

		For CSS, All html elements were considered as a box. This is our CSS Box Model. For Javascript, all HTML elements is considered as an object. This is what we call JS Document Object Model

		Since in JS, all html elements are objects and can be selected, we can manipulate and interactions to our web pages using javascript.

*/

//document is a keyword in JS which refers to the whole document. To the whole html page.
//console.log(document);

//store/select a particular element from our page to a variable.
//querySelector() is a method which can be used to select a specific element from our document. The querySelector() uses CSS-like notation/selector to select an element.
let firstNameLabel = document.querySelector("#label-first-name");
let lastNameLabel = document.querySelector("#label-last-name");

//Stored/selected the element with the id label-first-name. We can then manipulate the element using JS.



//We can access properties from our elements, because JS considers html elements as Objects.
//What is the output of the property .innerHTML from our firstNameLabel? First Name:
//innerHTML is a property of an element which considers all the children of the selected element as string. This includes other elments and text content.


//can we update the innerHTML property of an element?
/*let city = "Tokyo";
if(city ==="New York") {
	firstNameLabel.innerHTML = `I like New York City.`;

}
else {
	lastNameLabel.innerHTML = `I don't like New Your city, I like ${city}`;

}*/

/*
	Event Listener: 
	Event Listeners allow us to listen/observe for user interaction within our page.

	On the event that the user clicks on our firstNameLabel, we ran our anonymous function to perform a task,
	which is to change the innerHTML of our firstNameLabel to a different message.*/
firstNameLabel.addEventListener('click', () => {
	firstNameLabel.innerHTML = `I've been clicked.`;
	firstNameLabel.style.color = "red";
	firstNameLabel.style.fontSize = "10vh";

})

	let clickCount = 0;
lastNameLabel.addEventListener('click', () => {

	let result = "";

	lastNameLabel.innerHTML = "I'm red.";
	lastNameLabel.style.color = "red";
	lastNameLabel.style.fontSize = "5vh";

	clickCount ++;
	result += clickCount;

	if (clickCount % 2 === 0) {
		lastNameLabel.innerHTML = "I'm black.";
		lastNameLabel.style.color = "black";
		lastNameLabel.style.fontSize = "16px";
		}

})

function blah() {
	let firstname = document.querySelector('#txt-first-name').value;
	let lastname = document.querySelector('#txt-last-name').value ;
	let msg = document.querySelector("#errormsg").innerHTML;
if ( firstname !== "blue" || lastname !== "devera"){

	 console.log("Invalid name");
}
else {

	console.log(`Welcome ${firstname}!!`);
}

}
//keyup - is an event wherein we able to perform a task when the use lets go of a key. Keyup is best used in input elements that require key inputs.

//select the input element
let inputFirstName = document.querySelector("#txt-first-name");
let inputLastName = document.querySelector("#txt-last-name");
//.value is a property of mostly input elements. It contains the current value of the element.

//Initially, our input elements are empty.
//So, initially, our .value property for our inputFirstName is blank.
//Why is the .value still blank when we type into the input?
//This console log only outputs and ran the first time our page was shown, therefore, any changes you typed into the input element will not be shown, because console.log() only ran once.
//console.log(inputFirstName.value);

//To be able to log in the console, the current value of our input element as we type, we have to add an event.

//To log the current value of the inputFirstName every time we typed into the element.

/*inputFirstName.addEventListener('keyup',()=>{

	console.log(inputFirstName.value);

})*/


let fullNameDisplay = document.querySelector("#full-name-display");




/*	Synyax of addEventListener: 
element.addEventListener(<event>, <function>);

You can create/add named functions as the function for the addEventListener.	*/


const showName = () => {
	console.log(inputFirstName.value);
	fullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`;
	console.log(inputLastName.value);
}



inputFirstName.addEventListener ('keyup', showName);

//Multiple addEventListeners can run the same named function

inputLastName.addEventListener('keyup', showName);

